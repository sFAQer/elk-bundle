docker stop es-logs
docker rm es-logs

docker run --restart=always --name es-logs -d  \
   -p 9200:9200 -p 9300:9300 -v /srv/esdata:/usr/share/elasticsearch/data elasticsearch

docker stop kibana-logs
docker rm kibana-logs

docker run --restart=always --name kibana-logs --link es-logs:elasticsearch -p 5601:5601 -d kibana

docker stop logstash-to-es
docker rm logstash-to-es

docker run -d --restart=always --name logstash-to-es -v `pwd`/conf:/conf -p 5044:5044 logstash logstash -f /conf/logstash.conf

docker stop graphana-logs
docker rm graphana-logs

docker run -d --restart=always --name graphana-logs -p 3000:3000 \
    -v /srv/grafana:/var/lib/grafana --link es-logs:elasticsearch \
    -e "GF_SECURITY_ADMIN_PASSWORD=secret" \
    grafana/grafana:develop
