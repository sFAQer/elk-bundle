# Использование Elastic Search для 1С

репозиторий содержит базовые настройки по развертыванию необходимой инфраструктуры


## Использование разработчиком


* используется VirtualBox для развертывания стэка приложений


```
vagrant up
vagrant ssh
cd /vagrant
./build.sh
```

* для стэка редакции 2 используется `run.sh`
* для стэка редакции 5 используется `run-50.sh`

```
vagrant ssh
cd /vagrant
run-50.sh
```

> почему используется 5.0 - дело в том, что в 5.0 очень много изменений в части поддержки высокопроизводительных сборщиков GoLang (Beats)

## Использование специалистом инфраструктуры

устанавливаются плагины развертывания в конкретной виртуализированной среде

используем следующие провайдеры:

* https://github.com/ggiamarchi/vagrant-openstack-provider
* https://www.vagrantup.com/docs/hyperv/usage.html
* https://www.digitalocean.com/community/tutorials/how-to-use-digitalocean-as-your-provider-in-vagrant-on-an-ubuntu-12-10-vps  

пример установки плагина

```
vagrant plugin install vagrant-digitalocean
vagrant plugin install vagrant-openstack-provider
```

таким образом достигается развертывание одинаковое как у разработчика, так и у администратора
